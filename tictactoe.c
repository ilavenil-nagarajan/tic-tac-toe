#include<stdio.h>
#include<conio.h>
#include<unistd.h>
char board[10]={'0','1','2','3','4','5','6','7','8','9'};
void board_design();
int how_to_play();
int play();
int exit();

int main()
{

   int choice,flag=0;
 do{
   printf("\t\t\tTIC TAC TOE\n");
   printf("1. How to play");
   printf("\n2. Play");
   printf("\n3. Exit");
   printf("\n\nEnter your choice: ");
   scanf("%d",&choice);
   switch(choice)
   {
       case 1:
          flag=how_to_play();
          break;
       case 2:
           flag=play();
           break;
       case 3:
           flag=exit();
           break;
       default:
           {
               system("CLS");
               system("COLOR 0C");
               printf("\t\t\tINVALID CHOICE");
               break;
               flag=1;
           }
     }
   }while(flag==1);
   return 0;
}
int how_to_play()
{
    system("CLS");
    printf("\t\t\tINSTRUCTIONS");
    printf("\n1. The object of Tic Tac Toe is to get three in a row.");
    printf("\n2. You play on a three by three game board.");
    printf("\n3. The first player is known as X and the second is O.");
    printf("\n4. Players alternate placing Xs and Os on the game board until either oppent has three in a row or all nine squares are filled.");
    printf("\n\n\t\tEnter y or Y to return to main menu: ");
    char c;
    c=getche();
    if(c=='y'|| c=='Y')
    {
        system("CLS");
        return 1;
    }
    else
    {
        system("CLS");
        return 0;
    }
}
void board_design()
{
    system("CLS");

    printf("\t\t\t    |    |    \n");
    printf("\t\t\t %c  | %c  | %c \n",board[1],board[2],board[3]);
    printf("\t\t\t____|____|____\n");
    printf("\t\t\t    |    |    \n");
    printf("\t\t\t %c  | %c  | %c \n",board[4],board[5],board[6]);
    printf("\t\t\t____|____|____\n");
    printf("\t\t\t    |    |    \n");
    printf("\t\t\t %c  | %c  | %c \n",board[7],board[8],board[9]);
    printf("\t\t\t    |    |    \n");
}
int play()
{
    int player=1,i,square;
    char mark,player1[100],player2[100];
    system("CLS");
    printf("ENTER PLAYER 1 NAME: ");
    scanf("%s",player1);
    printf("ENTER PLAYER 2 NAME: ");
    scanf("%s",player2);
    do
    {
        board_design();
        player=(player%2)? 1:2;
        mark=(player==1)? 'X':'O';
        if(player==1)
        {
            system("COLOR 0A");
        }
        else{
            system("COLOR 0E");
        }
        if(player==1)
          printf("%s choose a square to place %c: ",player1,mark);
        else
          printf("%s choose a square to place %c: ",player2,mark);
        scanf("%d",&square);
        if(square==1 && board[1]=='1')
            board[1]=mark;
        else if(square==2 && board[2]=='2')
            board[2]=mark;
        else if(square==3 && board[3]=='3')
            board[3]=mark;
        else if(square==4 && board[4]=='4')
            board[4]=mark;
        else if(square==5 && board[5]=='5')
            board[5]=mark;
        else if(square==6 && board[6]=='6')
            board[6]=mark;
        else if(square==7 && board[7]=='7')
            board[7]=mark;
        else if(square==8 && board[8]=='8')
            board[8]=mark;
        else if(square==9 && board[9]=='9')
            board[9]=mark;
        else
        {
            system("COLOR 0C");
            printf("Invalid square\n");
            system("PAUSE");
            player--;
        }
        i=checkwin();
        player++;
    }while(i==-1);
  board_design();
    if(i==1)
    {
        system("CLS");

        if(--player==1)
        {
            system("COLOR 0A");
            printf("\n\t\t\t%s WON",player1);
        }
        else
        {
            system("COLOR 0E");
            printf("\n\t\t\t%s WON",player2);
        }
    }
    else
    {
        system("CLS");
        system("COLOR 0E");
        printf("\n\t\t\tGAME DRAW");
    }
    return 0;
}
int checkwin()
{
    if(board[1]==board[2]&& board[2]==board[3])
        return 1;
    else if(board[4]==board[5]&& board[5]==board[6])
        return 1;
    else if(board[7]==board[8] && board[8]==board[9])
        return 1;
    else if(board[1]==board[4] && board[4]==board[7])
        return 1;
    else if(board[2]==board[5] && board[5]==board[8])
        return 1;
    else if(board[3]==board[6] && board[6]==board[9])
        return 1;
    else if(board[1]==board[5] && board[5]==board[9])
        return 1;
    else if(board[3]==board[5] && board[5]==board[7])
        return 1;
    else if(board[1] != '1' && board[2] != '2' && board[3] != '3' &&
        board[4] != '4' && board[5] != '5' && board[6] != '6' && board[7]
        != '7' && board[8] != '8' && board[9] != '9')
        return 0;
    else
        return -1;
}
int exit()
{
    printf("\n\t\t\tARE YOU SURE YOU WANT TO QUIT ");
    printf("\nENTER Y OR y TO QUIT: ");
    char c;
    c=getche();
    if(c=='y' || c=='Y')
    {
        system("CLS");
        return 0;
    }
    else
    {
        system("CLS");
        return 1;
    }
    
}

